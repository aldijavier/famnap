{{--<!doctype html>--}}
{{--<html lang="en">--}}
{{--<head>--}}
{{--<meta charset="UTF-8">--}}
{{--<meta name="viewport"--}}
{{--content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">--}}
{{--<meta http-equiv="X-UA-Compatible" content="ie=edge">--}}
{{--<link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap/dist/css/bootstrap.min.css ')}}">--}}
{{--<!-- Font Awesome -->--}}
{{--<link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/css/font-awesome.min.css')}} ">--}}
{{--<!-- Ionicons -->--}}
{{--<link rel="stylesheet" href="{{ asset('assets/bower_components/Ionicons/css/ionicons.min.css')}} ">--}}

{{--<title>Product Masuk Exports All PDF</title>--}}
{{--</head>--}}
{{--<body>--}}
<style>
    #product-masuk {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #product-masuk td, #product-masuk th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #product-masuk tr:nth-child(even){background-color: #f2f2f2;}

    #product-masuk tr:hover {background-color: #ddd;}

    #product-masuk th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }
</style>

<table id="product" width="100%">
    <thead>
    <tr>
        <th>ID</th>
        <th>Nomor Form</th>
        <th>Jenis Kategori</th>
        <th>Nama Kategori</th>
        <th>Nama Barang</th>
        <th>Tanggal Kembali</th>
        <th>Lokasi Kembali</th>
        <th>Lokasi Pemasangan</th>
        <th>Departement</th>
        <th>Departement PIC</th>
        <th>SPK</th>
        <th>Project Form</th>
        <th>Quantity</th>
        <th>Serial Number</th>
        <th>Kondisi Barang</th>
        <th>Kondisi Barang Rusak</th>
        <th>Kondisi Barang Re-Use</th>
        <th>Image</th>
        <th>PIC</th>
    </tr>
    </thead>
    @foreach($product_return as $p)
        <tbody>
        <tr>
            <td>{{ $p->id }}</td>
            <td>{{ $p->nomor_form }}</td>
            <td> @if ($p->jenis_kategori === 1)
                <span>Asset</span>
            @else
                <span>Consumable</span>
            @endif</td>
            <td>{{ $p->nama_kategori }}</td>
            <td>{{ $p->product->nama }}</td>
            <td>{{ $p->tanggal_kembali }}</td>
            <td>{{ $p->lokasi_kembali }}</td>
            <td>{{ $p->lokasi_pemasangan }}</td>
            <td> @if ($p->departement === 1)
                <span>Customer Care</span>
            @elseif($p->departement === 2)
                <span>Sales Support</span>
            @elseif($p->departement === 3)
                <span>Corporate Marketing Communication</span>
            @elseif($p->departement === 4)
                <span>Corporate Marketing Communication</span>
            @elseif($p->departement === 5)
                <span>Wholesale Sales</span>
            @elseif($p->departement === 6)
                <span>Government Sales</span>
            @elseif($p->departement === 7)
                <span>Key Account Sales</span>
            @elseif($p->departement === 8)
                <span>Accounting & Tax</span>
            @elseif($p->departement === 9)
                <span>Finance</span>
            @elseif($p->departement === 10)
                <span>Budget Control</span>
            @elseif($p->departement === 11)
                <span>Revenue Assurance</span>
            @elseif($p->departement === 12)
                <span>Risk & Compliance</span>
            @elseif($p->departement === 13)
                <span>Property & Partnership</span>
            @elseif($p->departement === 14)
                <span>Procurement</span>
            @elseif($p->departement === 15)
                <span>Key Facility & Asset Management</span>
            @elseif($p->departement === 16)
                <span>System Development</span>
            @elseif($p->departement === 17)
                <span>IT & System Administrator</span>
            @elseif($p->departement === 18)
                <span>Network Development</span>
            @elseif($p->departement === 19)
                <span>Network Operation</span>
            @elseif($p->departement === 20)
                <span>Network Delivery</span>
            @elseif($p->departement === 21)
                <span>Network Quality Improvement</span>
            @elseif($p->departement === 22)
                <span>Data Center</span>
            @elseif($p->departement === 23)
                <span>Cable Landing Station</span>
            @elseif($p->departement === 24)
                <span>Legal & Regulatory</span>
            @elseif($p->departement === 25)
                <span>Human Capital</span>
            @endif</td>
            <td>{{ $p->departement_pic }}</td>
            <td>{{ $p->spk }}</td>
            <td>{{ $p->pform }}</td>
            <td>{{ $p->qty }}</td>
            <td>{{ $p->serial_number }}</td>
            <td>{{ $p->kondisibarang }}</td>
            <td>{{ $p->rusakondisi }}</td>
            <td>{{ $p->reusekondisi }}</td>
            <td>{{ $p->image }}</td>
            <td>{{ $p->pic }}</td>
        </tr>
        </tbody>
    @endforeach

</table>


{{--<!-- jQuery 3 -->--}}
{{--<script src="{{  asset('assets/bower_components/jquery/dist/jquery.min.js') }} "></script>--}}
{{--<!-- Bootstrap 3.3.7 -->--}}
{{--<script src="{{  asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }} "></script>--}}
{{--<!-- AdminLTE App -->--}}
{{--<script src="{{  asset('assets/dist/js/adminlte.min.js') }}"></script>--}}
{{--</body>--}}
{{--</html>--}}


