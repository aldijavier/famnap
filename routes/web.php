<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('dashboard', function () {
   return view('layouts.master');
});

// Route::get('/', [AuthController::class, 'showFormLogin'])->name('logins');
// Route::get('login', [AuthController::class, 'showFormLogin'])->name('logins');
// Route::post('login', [AuthController::class, 'login']);

Route::get('/redirect-inventory/{q}', '\vendor\laravel\framework\src\Illuminate\Foundation\@credentials');


Route::group(['middleware' => 'auth'], function () {
    Route::resource('categories','CategoryController');
    Route::get('/apiCategories','CategoryController@apiCategories')->name('api.categories');
    Route::get('/exportCategoriesAll','CategoryController@exportCategoriesAll')->name('exportPDF.categoriesAll');
    Route::get('/exportCategoriesAllExcel','CategoryController@exportExcel')->name('exportExcel.categoriesAll');
    Route::post('/importcategories','CategoryController@ImportExcel')->name('import.categories');

    Route::resource('customers','CustomerController');
    Route::get('/apiCustomers','CustomerController@apiCustomers')->name('api.customers');
    Route::post('/importCustomers','CustomerController@ImportExcel')->name('import.customers');
    Route::get('/exportCustomersAll','CustomerController@exportCustomersAll')->name('exportPDF.customersAll');
    Route::get('/exportCustomersAllExcel','CustomerController@exportExcel')->name('exportExcel.customersAll');

    Route::resource('sales','SaleController');
    Route::get('/apiSales','SaleController@apiSales')->name('api.sales');
    Route::post('/importSales','SaleController@ImportExcel')->name('import.sales');
    Route::get('/exportSalesAll','SaleController@exportSalesAll')->name('exportPDF.salesAll');
    Route::get('/exportSalesAllExcel','SaleController@exportExcel')->name('exportExcel.salesAll');

    Route::resource('suppliers','SupplierController');
    Route::get('/apiSuppliers','SupplierController@apiSuppliers')->name('api.suppliers');
    Route::post('/importSuppliers','SupplierController@ImportExcel')->name('import.suppliers');
    Route::get('/exportSupplierssAll','SupplierController@exportSuppliersAll')->name('exportPDF.suppliersAll');
    Route::get('/exportSuppliersAllExcel','SupplierController@exportExcel')->name('exportExcel.suppliersAll');

    Route::resource('products','ProductController');
    Route::get('/apiProducts','ProductController@apiProducts')->name('api.products');
    Route::get('/exportProductAll','ProductController@exportProductAll')->name('exportPDF.productAll');
    Route::get('/exportProductAllExcel','ProductController@exportExcel')->name('exportExcel.productAll');
    Route::get('dropdownlist/getstates/{id}','ProductController@getStates');
    Route::post('/importproducts','ProductController@ImportExcel')->name('import.products');

    Route::resource('productsOut','ProductKeluarController');
    Route::get('/apiProductsOut','ProductKeluarController@apiProductsOut')->name('api.productsOut');
    Route::get('/exportProductKeluarAll','ProductKeluarController@exportProductKeluarAll')->name('exportPDF.productKeluarAll');
    Route::get('/exportProductKeluarAllExcel','ProductKeluarController@exportExcel')->name('exportExcel.productKeluarAll');
    Route::get('/exportProductKeluar/{id}','ProductKeluarController@exportProductKeluar')->name('exportPDF.productKeluar');
    Route::get('dropdownlist/getstates2/{id}','ProductKeluarController@getStates2');
    Route::get('dropdownlist/getstates3/{id}','ProductKeluarController@getStates3');
    Route::get('/product_keluar/detail/{id}', 'ProductKeluarController@detail');
    Route::get('books/{id}/downloadspk', 'ProductKeluarController@download')->name('books.downloadspk');
    Route::get('books/{id}/downloadpfrom', 'ProductKeluarController@downloadDO')->name('books.downloadpfrom');


    Route::resource('productsIn','ProductMasukController');
    Route::get('/apiProductsIn','ProductMasukController@apiProductsIn')->name('api.productsIn');
    Route::get('/exportProductMasukAll','ProductMasukController@exportProductMasukAll')->name('exportPDF.productMasukAll');
    Route::get('/exportProductMasukAllExcel','ProductMasukController@exportExcel')->name('exportExcel.productMasukAll');
    Route::get('/exportProductMasuk/{id}','ProductMasukController@exportProductMasuk')->name('exportPDF.productMasuk');
    Route::get('dropdownlist/getstates1/{id}','ProductMasukController@getStates1');
    Route::get('/product_masuk/detail/{id}', 'ProductMasukController@detail');
    Route::get('books/{id}/download', 'ProductMasukController@download')->name('books.download');
    Route::get('books/{id}/downloadDO', 'ProductMasukController@downloadDO')->name('books.downloadDO');
    Route::get('/productsIn/cari','ProductMasukController@cari');

    Route::resource('productsReturn','ProductReturnController');
    Route::get('/apiProductsReturn','ProductReturnController@apiProductsReturn')->name('api.productsReturn');
    Route::get('/exportProductReturnAll','ProductReturnController@exportProductReturnAll')->name('exportPDF.productReturnAll');
    Route::get('/exportProductReturnAllExcel','ProductReturnController@exportExcel')->name('exportExcel.productReturnAll');
    Route::get('/exportProductReturn/{id}','ProductReturnController@exportProductReturn')->name('exportPDF.productReturn');
    // Route::get('dropdownlist/getstates2/{id}','ProductReturnController@getStates2');
    // Route::get('dropdownlist/getstates3/{id}','ProductReturnController@getStates3');
    Route::get('/product_return/detail/{id}', 'ProductReturnController@detail');
    Route::get('books/{id}/downloadspkReturn', 'ProductReturnController@downloadspkReturn')->name('books.downloadspkReturn');
    Route::get('books/{id}/downloadpfromReturn', 'ProductReturnController@downloadpfromReturn')->name('books.downloadpfromReturn');
    Route::get('books/{id}/downloadimageReturn', 'ProductReturnController@downloadimageReturn')->name('books.downloadimageReturn');

    //Export
    Route::get('/tickets/rfo/{id}', 'App\Http\Controllers\ExportController@rfo')->middleware(['checkRole:Super Admin,User,Customer Care,BOD']);
    Route::get('/tickets/rfo_maintenance/{id}', 'App\Http\Controllers\ExportController@rfoMaintenance')->middleware(['checkRole:Super Admin,User,Customer Care,BOD']);
    Route::post('/tickets/export', 'ExportController@export');
    Route::post('/tickets/exportkeluar', 'ExportController@exportkeluar');
    Route::post('/tickets/exportreturn', 'ExportController@exportreturn');
    Route::post('/tickets/exportLogAssign', 'App\Http\Controllers\ExportController@exportLogAssign')->middleware(['checkRole:Super Admin,User,Customer Care,BOD']);
    Route::post('/tickets_internal/exportLogAssign', 'App\Http\Controllers\ExportController@exportLogAssign')->middleware(['checkRole:Super Admin,User,Customer Care,BOD']);
    Route::post('/tickets/export-bulk-Assign', 'App\Http\Controllers\ExportController@exportBulkAssign')->middleware(['checkRole:Super Admin,User,Customer Care,BOD']);
    //End export
});

