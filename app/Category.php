<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = ['kode_barang', 'name', 'spek', 'brand', 'category'];


public function listcategory()
    {
        return $this->belongsTo(ListCategory::class);
    }
}