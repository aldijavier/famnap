<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListCategory extends Model
{
    protected $table = 'list_categories';
    protected $fillable = ['name'];
}
