<?php

namespace App\Http\Controllers;

use App\Category;
use App\Customer;
use App\Exports\ExportProdukReturn;
use App\Product;
use App\Product_Return;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use PDF;
use App\Departement;
use App\UserDemand;
use DB;
use Carbon\Carbon;
use App\Location;
use Response;

class ProductReturnController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin,staff');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryz = DB::table('categories')
            ->get()
            ->pluck('name','id');
        
        $productsz = DB::table('list_categories')->pluck("name","id");

        $category = Category::orderBy('name','ASC')
            ->get()
            ->pluck('name','id');

        // $products = Product::orderBy('nama','ASC')
        //     ->get()
        //     ->pluck('nama','id');
        
        $location = Location::orderBy('lokasi')
            ->get()
            ->pluck('lokasi', 'lokasi');
        
        $departements = DB::table('departements')->pluck("nama_departement","id");

        $departementspic = DB::table('user_demands')
            ->get()
            ->pluck('nama_karyawan','id');

        $deptpic = UserDemand::orderBy('nama_karyawan','ASC')
        ->get()
        ->pluck('nama_karyawan','id');

        $productawal = Product::orderBy('nama','ASC')
        ->get()
        ->pluck('nama','id');
        // $customers = Customer::orderBy('nama','ASC')
        //     ->get()
        //     ->pluck('nama','id');
        $productakhir = DB::table('products')
            ->get()
            ->pluck('nama','id');

        $awal = date('Y-m-d', strtotime('-1 days'));
        $akhir = date('Y-m-d');

        $invoice_data = Product_Return::all();
        return view('product_return.index', compact('awal', 'akhir','invoice_data', 'categoryz', 'location', 'productsz', 'category', 'departements', 'departementspic', 'deptpic', 'productawal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function downloadspkReturn($uuid)
    {
        $book = Product_Return::where('id', $uuid)->firstOrFail();
        $pathToFile = public_path('spkReturn'. '/' . $book->spk);
        return response()->download($pathToFile);
    }

    public function downloadpfromReturn($uuid)
    {
        $book = Product_Return::where('id', $uuid)->firstOrFail();
        $pathToFile = public_path('pfromReturn'. '/' . $book->pform);
        return response()->download($pathToFile);
    }

    public function downloadimageReturn($uuid)
    {
        $book = Product_Return::where('id', $uuid)->firstOrFail();
        $pathToFile = public_path('imageReturn'. '/' . $book->image);
        return response()->download($pathToFile);
    }

    public function getStates3($id) 
    {
        // $states = \DB::select("select * from products where qty >= 1 and jenis_id = 2");        
        $states = DB::table("products")
        ->where("category_id", $id)
        ->where ('qty', '>', 'minimal_qty')
        ->pluck("nama");
        return json_encode($states);
    }

    public function getStates2($id) 
    {        
        $states = DB::table("user_demands")->where("dept",$id)->pluck("nama_karyawan", "nama_karyawan");
        return json_encode($states);
    }

    public function detail($id)
    {
        $title = 'Detail Produk Return';
        $dt = Product_Return::find($id);
        $dept = Departement::find($id);
        $dept2 = Departement::all();
        $invoice_data = Product_Return::all();

        return view('product_return.detail', compact('title', 'dt', 'invoice_data', 'dept', 'dept2'));
    }

    public function store(Request $request)
    {
        
        $this->validate($request, [
        //    'pic'     => 'required',
        //    'jenis_kategori'    => 'required',
        //    'nama_kategori'            => 'required',
        //    'product_id'           => 'required',
        //    'lokasi_pemasangan'           => 'required',
        //    'departement'           => 'required',
        //    'departement_pic'           => 'required',
        ]);
        
        // Product_Masuk::create($request->all());
        $product=Product_Return::create([
            'pic' => $request->pic,
            'jenis_kategori' => $request->jenis_kategori,
            'nama_kategori' => $request->nama_kategori,
            'product_id' => $request->product_id,
            'tanggal_kembali' => $request->tanggal_kembali,
            'lokasi_kembali' => $request->lokasi_kembali,
            'lokasi_pemasangan' => $request->lokasi_pemasangan,
            'departement' => $request->departement,
            'departement_pic' => $request->departement_pic,
            'qty' => $request->qty,
            'kondisibarang' => $request->kondisibarang,
            'rusakondisi' => $request->rusakondisi,
            'reusekondisi' => $request->reusekondisi,
        ]);
        

        if($product){
            $created_date=Carbon::now(); 
        // use within single line code
            // error_log('Some message here.');
            $id=$product->id;
            //upload file
            if($request->file('spk')){
                $extension1 = $request->file('spk')->getClientOriginalExtension();
                $doc_name1 = $id.'spk.'.$extension1;
                $store1 = $request->file('spk')->storeAs('spkReturn', $doc_name1);
            } else{
                $doc_name1="";
            }
                
            if($request->file('pform')){
                $extension2 = $request->file('pform')->getClientOriginalExtension();
                $doc_name2 =  $id.'pform.'.$extension2;
                $store2 = $request->file('pform')->storeAs('pformReturn', $doc_name2);
            }
            else{
                $doc_name2="";
            }

            if($request->file('image')){
                $extension2 = $request->file('image')->getClientOriginalExtension();
                $doc_name3 =  $id.'image.'.$extension2;
                $store3 = $request->file('image')->storeAs('imageReturn', $doc_name3);
            }
            else{
                $doc_name3="";
            }
                
            $insert_filename=Product_Return::where('id',$id)
            ->update([
                'spk' => $doc_name1,
                'pform' => $doc_name2,
                'image' => $doc_name3,
            ]);
                
            $period_ticket = $created_date->format('ymd');
            //no urut akhir
            $noticket="$id"."/"."FTBR-NAP"."/"."$period_ticket";
            
            
            $create_form=Product_Return::where('id',$id)
            ->update([
                'nomor_form' => $noticket
            ]);
         
            // $product = Product_Keluar::create($request->all());
            $product1 = Product::findOrFail($request->product_id);

            if($request->jenis_kategori == 1){
                if($request->kondisibarang == "rusak") {
                    $product1->qty_rusak += $request->qty;
                    $product1->save();
                    return response()->json([
                        'success'    => true,
                        'message'    => 'Product Asset rusak berhasil dikembalikan'
                    ]);
                } else if($request->kondisibarang == "reuse"){
                    $product1->qty_return += $request->qty;
                    $product1->save();
                    return response()->json([
                        'success'    => true,
                        'message'    => 'Produk barang re-use berhasil dikembalikan'
                    ]);
                } else if($request->kondisibarang == "normal"){
                    $product1->qty += $request->qty;
                    $product1->save();
                    return response()->json([
                        'success'    => true,
                        'message'    => 'Produk barang normal berhasil dikembalikan'
                    ]);
                } else{
                    $product1->save();
                    return response()->json([
                        'success'    => true,
                        'message'    => 'Product Asset normal berhasil dikembalikan'
                    ]);
                }
            } else if($request->jenis_kategori == 2){
                if($request->kondisibarang == "rusak") {
                    $product1->qty_rusak	 += $request->qty;
                    $product1->save();
                    return response()->json([
                        'success'    => true,
                        'message'    => 'Produk barang rusak berhasil dikembalikan'
                    ]);
                } else if($request->kondisibarang == "reuse"){
                    $product1->qty_return += $request->qty;
                    $product1->save();
                    return response()->json([
                        'success'    => true,
                        'message'    => 'Produk barang re-use berhasil dikembalikan'
                    ]);
                } else if($request->kondisibarang == "normal"){
                    $product1->qty += $request->qty;
                    $product1->save();
                    return response()->json([
                        'success'    => true,
                        'message'    => 'Produk barang normal berhasil dikembalikan'
                    ]);
                }
            }
        }
    }
        
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_return = Product_Return::find($id);
        return $product_return;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            // 'product_id'     => 'required',
            // 'customer_id'    => 'required',
            // 'qty'            => 'required',
            // 'tanggal'           => 'required'
        ]);

        $product_return = Product_Return::findOrFail($id);
        $product = $product_return->update($request->all());

        $created_date=Carbon::now(); 
        if($product){
            // use within single line code
            error_log('Some message here.');
            $id=$product_return->id;

            //upload file
            if($request->file('spk')){
                $extension1 = $request->file('spk')->getClientOriginalExtension();
                $doc_name1 = $id.'spk.'.$extension1;
                $store1 = $request->file('spk')->storeAs('spk', $doc_name1);
            }
            else{
                $doc_name1="";
            }

            if($request->file('pform')){
                $extension2 = $request->file('pform')->getClientOriginalExtension();
                $doc_name2 =  $id.'pform.'.$extension2;
                $store2 = $request->file('pform')->storeAs('project_form', $doc_name2);
            }
            else{
                $doc_name2="";
            }

            if($request->file('image')){
                $extension2 = $request->file('image')->getClientOriginalExtension();
                $doc_name3 =  $id.'image.'.$extension2;
                $store3 = $request->file('image')->storeAs('imageReturn', $doc_name3);
            }
            else{
                $doc_name3="";
            }
                
            $insert_filename=Product_Return::where('id',$id)
            ->update([
                'spk' => $doc_name1,
                'pform' => $doc_name2,
                'image' => $doc_name3,
            ]);
            
            $period_ticket = $created_date->format('ymd');
            //no urut akhir
            $noticket="$id"."/"."FTBR-NAP"."/"."$period_ticket";
            
            
            $create_form=Product_Return::where('id',$id)
            ->update([
                'nomor_form' => $noticket
            ]);

        $product = Product::findOrFail($request->product_id);
        if($request->jenis_kategori == 1){
            if($request->kondisibarang == "rusak") {
                $product1->qty_rusak += $request->qty;
                $product1->save();
                return response()->json([
                    'success'    => true,
                    'message'    => 'Product Asset rusak berhasil dikembalikan'
                ]);
            } else if($request->kondisibarang == "reuse"){
                $product1->qty_return += $request->qty;
                $product1->save();
                return response()->json([
                    'success'    => true,
                    'message'    => 'Produk barang re-use berhasil dikembalikan'
                ]);
            } else if($request->kondisibarang == "normal"){
                $product1->qty += $request->qty;
                $product1->save();
                return response()->json([
                    'success'    => true,
                    'message'    => 'Produk barang normal berhasil dikembalikan'
                ]);
            } else{
                $product1->save();
                return response()->json([
                    'success'    => true,
                    'message'    => 'Product Asset normal berhasil dikembalikan'
                ]);
            }
        } else if($request->jenis_kategori == 2){
            if($request->kondisibarang == "rusak") {
                $product1->qty_rusak	 += $request->qty;
                $product1->save();
                return response()->json([
                    'success'    => true,
                    'message'    => 'Produk barang rusak berhasil dikembalikan'
                ]);
            } else if($request->kondisibarang == "reuse"){
                $product1->qty_return += $request->qty;
                $product1->save();
                return response()->json([
                    'success'    => true,
                    'message'    => 'Produk barang re-use berhasil dikembalikan'
                ]);
            } else if($request->kondisibarang == "normal"){
                $product1->qty += $request->qty;
                $product1->save();
                return response()->json([
                    'success'    => true,
                    'message'    => 'Produk barang normal berhasil dikembalikan'
                ]);
            }

        return response()->json([
            'success'    => true,
            'message'    => 'Product Out Updated'
        ]);
    }
    }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product_Return::destroy($id);

        return response()->json([
            'success'    => true,
            'message'    => 'Products Delete Deleted'
        ]);
    }



    public function apiProductsReturn(){
        $product = Product_Return::all();
        $pic = UserDemand::all();
        $departement = Departement::all();

        return Datatables::of($product)
            ->addColumn('products_name', function ($product){
                return $product->product->nama;
            })
            ->addColumn('show_photo', function($product){
                if ($product->image == NULL){
                    return 'No Image';
                }
                return '<img class="rounded-square" width="50" height="50" src="'.  url('imageReturn'. '/' . $product->image) .'" alt="">';
            })
            // ->addColumn('dept_pic', function ($pic){
            //     return $pic->product->nama_karyawan;
            // })
            ->addColumn('action', function($product){
                return '<a href="/inventory/product_return/detail/'. $product->id .'" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>' .
                    '<a onclick="editForm('. $product->id .')" class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-edit"></i></a> ';
            })
            ->rawColumns(['products_name','action','show_photo'])->make(true);

    }

    
                    // .'<a onclick="deleteData('. $product->id .')" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></a>'

    public function exportProductReturnAll()
    {
        $product_return = Product_Return::all();
        $pdf = PDF::loadView('product_return.productReturnAllPDF',compact('product_return'));
        return $pdf->download('product_return.pdf');
    }

    public function exportProductReturn($id)
    {
        $product_return = Product_Return::findOrFail($id);
        $pdf = PDF::loadView('product_return.productReturnPDF', compact('product_return'));
        return $pdf->download($product_return->id.'_product_return.pdf');
    }

    public function exportExcel()
    {
        return (new ExportProdukReturn)->download('product_return.xlsx');
    }
}
