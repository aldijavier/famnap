<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_Return extends Model
{
    protected $table = 'product_return';

    protected $fillable = ['nomor_form', 'pic','jenis_kategori','nama_kategori',
    'product_id','tanggal_kembali','lokasi_kembali', 'lokasi_pemasangan', 'departement', 'departement_pic',
    'spk', 'pform', 'qty','kondisibarang','rusakondisi', 'reusekondisi', 'image'];

    protected $hidden = ['created_at','updated_at'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function departement()
    {
        return $this->belongsTo(Departement::class, 'departement');
    }

    

    // public function customer()
    // {
    //     return $this->belongsTo(Customer::class);
    // }
}
